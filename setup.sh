#!/bin/sh
set -ex
SLAVENAME="arm-slave"
PASSWORD=pass
virtualenv --no-site-packages slavenv
sed -i "s/SLAVENAME/$SLAVENAME/" start
cd slavenv
./bin/pip install buildbot-slave
./bin/pip install requests
./bin/pip install sandboxlib
./bin/buildslave create-slave --umask=022 -r $SLAVENAME 52.19.1.31:9989 $SLAVENAME $PASSWORD
